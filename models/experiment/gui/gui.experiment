/***
* Name: gui
* Author: francoisledoyen
* Description: This wizard creates a new experiment file.
* Tags: Tag1, Tag2, TagN
***/

model diffusion_innovation_argumentation 
import "../../main.gaml"

experiment diffusion_innovation_argumentation {
	parameter "Number of agents :" var: nb_agents <- 100 category: "General";
	parameter "Adoption threshold : " var: p min: 0 category: "General";
	parameter "Probability to getting the message : " var: omega min: 0.0 max: 1.0 category: "General";
	
	parameter "Number of arguments : " var: nb_args <- 20 category: "Creation of the argumentation graph";
	parameter "Proportion of pro arguments : " var: prop_pro_args category: "Creation of the argumentation graph";
	parameter "Proportion of attacks pro -> con : " var: prop_att_p_c category: "Creation of the argumentation graph";
	parameter "Proportion of attacks con -> pro : " var: prop_att_c_p category: "Creation of the argumentation graph";
	
	
	output {
		browse "individus" value: list(individual) attributes: [
																	"name", "type", "informed", "decision", "interest", 
																	"global_op", "global_op_u",
																	"indiv_ben", "indiv_ben_u", 
																	"social_op", "social_op_u", "diffuse", "init_glob_op", "n_concerned_in_exchange"
																];
		
		display chart refresh: every(25 #cycle){
			
			
			chart "opinon"  series_label_position:none memorize: false   {
				datalist legend:list(individual) collect each.name  value: list(individual) collect (each.global_op) color: [#black] marker: false;
			}
			
			
			chart "decision histogram" type: histogram size: {1,0.5} position: {0, 0.5} y_range: {0,nb_agents}{
				data "not concerned" value: individual count (each.state = "not concerned") color:#grey; 
				data "no adoption" value: individual count (each.state = "no adoption") color:#red;
				data "adoption" value: individual count (each.state = "adoption") color:#green;
				data "satisfied" value: individual count (each.state = "satisfied") color:#green;
				data "unsatisfied" value: individual count (each.state = "unsatisfied") color:#red;
			}
		}
		
		monitor "Not-concerned" value: individual count (each.state = "not concerned");
		monitor "Information request" value: individual count (each.state = "information request");
		monitor "No adoption" value: individual count (each.state = "no adoption");
		monitor "Pre-adoption" value: individual count (each.state = "pre-adoption");
		monitor "Adoption" value: individual count (each.state = "adoption");
		monitor "Satisfied" value: individual count (each.state = "satisfied");
		monitor "Unsatisfied" value: individual count (each.state = "unsatisfied");
		
	}	
}
