import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def read_csv(file, output):
    data = pd.read_csv(file, sep=",")

    nb_attacks = list(np.unique(data["nb_attacks"].to_numpy()))
    legend = data.columns[1:]

    nchart = len(nb_attacks)-2
    fig, ax = plt.subplots(2, nchart//2, sharey='row', figsize=(15,10))
    plt.subplots_adjust(hspace=.5)
    
    for i in range(nchart):
        n = nb_attacks[i]
        d = data.loc[data["nb_attacks"]==n].drop(axis=1, labels="nb_attacks")

        mean = d.mean(axis=0).to_numpy()
        std_dev = d.std(axis=0).to_numpy()
        y = int(i%(nchart/2))
        x = 0 if i < nchart/2 else 1
        
        x_pos = np.arange(len(legend))
        ax[x, y].bar(x_pos, mean, yerr=std_dev, align='center', alpha=0.5, ecolor='black', capsize=2)
        # ax.set_xlabel('Individual benefit of agents')

        # ax.set_ylabel('Proportion of population (%)')
        ax[x, y].set_title('{} attacks'.format(n))
        ax[x, y].set_xticks(x_pos)
        ax[x, y].set_xticklabels(legend, fontsize=10, rotation=90)

        ax[x, y].yaxis.grid(True)

    # plt.show()
    plt.tight_layout()
    plt.savefig(output)


def bar_chart(data, legend, position):
    ax = data.plot.bar(ax=position)
    title = "{} attacks".format(int(legend["nb_attacks"]))
    ax.set_title(title)
    ax.set_ylabel("Population (%)", fontsize=5)
    ax.legend(handles=None)


if __name__ == '__main__':
    
    files = ["data/avec_media_diffusion.csv", "data/avec_media_diffusion.init.csv"]
    outputs = ["output/avec_media_diffusion.png", "output/avec_media_diffusion.init.png"]
    for i in range(len(files)):
        read_csv(files[i], outputs[i])

    




