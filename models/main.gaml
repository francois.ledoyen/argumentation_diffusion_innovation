/***
* Name: model
* Author: francoisledoyen
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model diffusion_innovation_argumentation

import "util/generator/individual_generator.gaml"
import "util/generator/graph_generator.gaml"

global {
	int nb_agents <- 60;
		
	float omega <- 0.5;
	
	string network_density <- "low" among: ["high", "low"];
	
	int p <- 15;
	
	int nb_args <- 60 parameter:true;
	list<argument> P;
	list<argument> C;
	
	list<string> criterion;
	list<string> sources;
	
	list<argument> arguments;	
	graph<argument, unknown> global_args_graph;
	
	int nb_attacks <- 15 parameter:true;
	
	float prop_att_p_c <- 0.5 parameter:true;
	float prop_att_c_p <- 0.5 parameter:true;

	int init_nb_p_ag <- 5;
	int init_nb_c_ag <- 5;
	int nb_relevents_args <- 7; 
	string scenario <- "random" among: ["random", "influencors", "isolated"];
	
	// VARS EXPE
	map<int, float> mean_op_times <- map<int, float>(map([]));
	argument new_arg;
	list<individual> pop;
	float prop_pop <- 0.1;
	float mean_op_init;
	float mean_u_init;
	list<individual> with_new_arg;
	// end VAR EXPE
	init {
		csv_file args_file <- csv_file("../includes/raw_args.csv", ",", true);
		file arg_graph_file <- file("../includes/args_graph.dl");
		arguments <- self.loadMyChoiceArgs(args_file);
		global_args_graph <- load_graph(arg_graph_file, arguments);
		
//		create argumentation_graph_generator returns: arg_graph_gen with: (
//			nb_args:nb_args,
//			prop_P_args:prop_pro_args, 
//			prop_att_P_C:prop_att_p_c,
//			prop_att_C_P:prop_att_c_p,
//			proportion:false,
//			nb_attacks:nb_attacks
//		);
//		global_args_graph <- arg_graph_gen[0].argumentation_graph;
//	 	P <- arg_graph_gen[0].P;
//	 	C <- arg_graph_gen[0].C;
//	 	
//	 	
	 	create individual_generator with: (
	 		nb_relevents_args:nb_relevents_args,
	 		global_args_graph:global_args_graph,
	 		source_types:self.sources,
	 		criteria:self.criterion
	 	);
	 	
	 	ask individual_generator{
	 		do create_individuals(nb_agents, 3, 3);
	 		do link_individuals;	
	 	}
	 	
//	 	
	 	
	 	// Init experiment
	 	
	 	string source <- "institution";
	 	ask individual {
	 		self.source_confidence[source] <- rnd(0.0, 1.0);
	 	}
	 	string criteria <- "Financier";
	 	new_arg <- argument(["id"::"0", 
									"conclusion"::"+", 
									"source_type"::source, 
									"criteria"::[criteria::1.0]]
									);
		
		add node(new_arg) to: global_args_graph;
		int nb_att <- 5;
		list<argument> args_attacked <- nb_att among  (global_args_graph.vertices where ((argument(each).conclusion = "-" and (criteria in each.criteria.keys))));
		
		if (false){
			loop ag over: args_attacked {
				global_args_graph <- global_args_graph add_edge(new_arg::ag); 
			}
		
			int nb <- int(prop_pop * length(individual));
			
			if scenario = "random" {
				pop <- (nb among individual);
			} else if scenario = "influencors" {
				pop <- (nb last_of (individual sort_by (length(each.neighbors))));
			} else if scenario = "isolated" {
				pop <- (nb first_of (individual sort_by (length(each.neighbors))));
			} 
			
			ask pop {
				do add_arg(new_arg);
			}
		}
		
	  	
	  	// end init experiment
	 	
	 	
	 	ask individual {
	 		social_op <- gauss(-0.5, 0.2);
	 		social_op_u <- gauss(0.4, 0.2);
			
			do compute_indiv_ben;
			do compute_indiv_ben_u;
			
			do compute_global_op;
			do compute_global_op_u;
			
			self.init_ben <- social_op;
		}
		
		mean_op_init <- individual mean_of each.global_op; 
		mean_u_init <- individual mean_of each.global_op_u;
		
	}

	list<argument> loadMyChoiceArgs(csv_file args_file){
		list<argument> args <- [];
		matrix data <- matrix(args_file);
		loop i from: 0  to: data.rows-1{
			string id <- data[0, i];
			string ccl <- data[3, i];
			string st <- data[16, i];
			string crit <- data[4,i];
			
			new_arg <- argument(["id"::id, 
									"conclusion"::ccl,
									"source_type"::st, 
									"criteria"::[crit::1.0]]
									);
									
			if !(crit in self.criterion){
				add crit to: criterion;
			}
			
			if !(st in self.sources){
				add st to: sources;
			} 
			
			add new_arg to: args;			
		}		
		return args;
	}

	reflex media_diffusion{
		ask individual where (each.state="information request"){
			if flip(omega){
				do add_arg(one_of(myself.global_args_graph.vertices));
				
				do compute_social_op;
				do compute_social_op_u;
				
				do compute_indiv_ben;
				do compute_indiv_ben_u;
				
				do compute_global_op;
				do compute_global_op_u;
			}
		}
	}
	
	reflex count_with_new_arg {
		loop a over: list(individual where (new_arg in each.argumentation_graph.vertices)){
			if !(a in  with_new_arg){
				add a to: with_new_arg;
			}
		}
	} 
	
	reflex dynamic {
		ask individual {
			if flip(0.7){
				do discuss();
			}
			
			
			do compute_social_op;
			do compute_social_op_u;
			
			do compute_indiv_ben;
			do compute_indiv_ben_u;
			
			do compute_global_op;
			do compute_global_op_u;
		}
	}
		
	reflex end_simulation when: cycle = 1000 {
		//do pause;
	}
	
	reflex write_mean_op when:every(500#cycle) {
		mean_op_times[cycle] <- individual mean_of each.global_op;
	} 
	
	 	
	
}

		
experiment main type: gui;


experiment "influence of number of attacks on opinion" type: batch until: cycle = 10000 repeat: 5 keep_seed: false{
	parameter "Number of agents :" var: nb_agents <- 100;
	parameter "Number of args" var: nb_args <- 60;
	parameter "Number of attacks" var: nb_attacks <- 0 among: [0, 100, 200, 300, 400, 500, 600, 700, 800, 900];
	int cpt <- 0;
	string directory <- "../results/influence_nb_attacks/data/";
	string name <- 'avec_media_diffusion';
	string file_name <- directory+name+".csv";
	string init_file_name <- directory+name+'.init.csv';
	
	
	reflex end_of_runs {
		int nb_simulations <- length(list(simulations));
		
		if cpt = 0 { save "nb_attacks,[-1;-0.75[,[-0.75;-0.5[,[-0.5;-0.25[,[-0.25;0.0[,[0.0;0.25[,[0.25;0.5[,[0.5;0.75[,[0.75;1.0]" type:"text" to: file_name header:false rewrite: true;}
		if cpt = 0 { save "nb_attacks,[-1;-0.75[,[-0.75;-0.5[,[-0.5;-0.25[,[-0.25;0.0[,[0.0;0.25[,[0.25;0.5[,[0.5;0.75[,[0.75;1.0]" type:"text" to: init_file_name header:false rewrite: true;}
		ask simulations {
		save [ 	nb_attacks, 	
				individual count (each.global_op < -0.75),
				individual count ((each.global_op >= -0.75) and (each.global_op < -0.5)),
				individual count ((each.global_op >= -0.5) and (each.global_op < -0.25)),
				individual count ((each.global_op >= -0.25) and (each.global_op < 0.0)),
				individual count ((each.global_op >= 0.0) and (each.global_op < 0.25)),
				individual count ((each.global_op >= 0.25) and (each.global_op < 0.5)),
				individual count ((each.global_op >= 0.5) and (each.global_op < 0.75)),
				individual count ((each.global_op >= 0.75))] 
				type:"csv" to: myself.file_name header: false rewrite: false;
		
		
		save [ 	nb_attacks, 	
				individual count (each.init_ben < -0.75),
				individual count ((each.init_ben >= -0.75) and (each.init_ben < -0.5)),
				individual count ((each.init_ben >= -0.5) and (each.init_ben < -0.25)),
				individual count ((each.init_ben >= -0.25) and (each.init_ben < 0.0)),
				individual count ((each.init_ben >= 0.0) and (each.init_ben < 0.25)),
				individual count ((each.init_ben >= 0.25) and (each.init_ben < 0.5)),
				individual count ((each.init_ben >= 0.5) and (each.init_ben < 0.75)),
				individual count ((each.init_ben >= 0.75))] 
				type:"csv" to: myself.init_file_name header: false rewrite: false;
		}
		cpt <- cpt + 1;
	}
}	

experiment institutioninforamaitondiffusion  type:batch repeat: 10 until: (cycle > 10000) keep_seed: false {
	parameter "Scenario" var: scenario <- "random" among: ["random", "influencors", "isolated"];
	
	int cpt <- 0;
	string directory <- "../results/scenario1/data/";
	string name <- "sans_arg_sans_soc";
	string file_name <- directory+name+".opinion.csv";
	string init_file_name <- directory+name+'.init.csv';
	string  data_file <- directory+name+'.table.csv';
	string time_file <- directory+name+".time.csv";
	
	string time_header{
		string res <- "";
		loop c from: 0 to: 10000 step: 500{
			res <- string(c)+",";
		}
		return res;
	}
	
	string list_to_csv(list l){
		string res <- '';
		loop e over:l{
			res <- res + string(e)+",";
		}
		return res;
	}
	
	string list_to_csv(list l){
		string res <- '';
		loop e over:l{
			res <- res + string(e)+",";
		}
		return res;
	}
	
	reflex end_of_runs {
		int nb_simulations <- length(list(simulations));
		
		if cpt = 0 { 
			save "scenario,[-1;-0.75[,[-0.75;-0.5[,[-0.5;-0.25[,[-0.25;0.0[,[0.0;0.25[,[0.25;0.5[,[0.5;0.75[,[0.75;1.0]" type:"text" to: file_name header:false rewrite: true;
			save "scenario,[-1;-0.75[,[-0.75;-0.5[,[-0.5;-0.25[,[-0.25;0.0[,[0.0;0.25[,[0.25;0.5[,[0.5;0.75[,[0.75;1.0]" type:"text" to: init_file_name header:false rewrite: true;
			save "scenario,mean_opinion,evolution,mean_uncertainty, evolution_u,nb_adoptants,nb_agents_with_new_arg" type:"text" to: data_file header:false rewrite: true;
			save "scenario,"+time_header() type:"text" to: time_file header:false rewrite: true;
		}
		
		
		ask simulations {
			save [ 	scenario, 	
					individual count (each.global_op < -0.75),
					individual count ((each.global_op >= -0.75) and (each.global_op < -0.5)),
					individual count ((each.global_op >= -0.5) and (each.global_op < -0.25)),
					individual count ((each.global_op >= -0.25) and (each.global_op < 0.0)),
					individual count ((each.global_op >= 0.0) and (each.global_op < 0.25)),
					individual count ((each.global_op >= 0.25) and (each.global_op < 0.5)),
					individual count ((each.global_op >= 0.5) and (each.global_op < 0.75)),
					individual count ((each.global_op >= 0.75))] 
					type:"csv" to: myself.file_name header: false rewrite: false;
			
			
			save [ 	scenario, 	
					individual count (each.init_ben < -0.75),
					individual count ((each.init_ben >= -0.75) and (each.init_ben < -0.5)),
					individual count ((each.init_ben >= -0.5) and (each.init_ben < -0.25)),
					individual count ((each.init_ben >= -0.25) and (each.init_ben < 0.0)),
					individual count ((each.init_ben >= 0.0) and (each.init_ben < 0.25)),
					individual count ((each.init_ben >= 0.25) and (each.init_ben < 0.5)),
					individual count ( (each.init_ben >= 0.5) and (each.init_ben < 0.75)),
					individual count ((each.init_ben >= 0.75))] 
					type:"csv" to: myself.init_file_name header: false rewrite: false;
					
			float mean_op <- individual mean_of (each.global_op);
			float evol <- mean_op - mean_op_init;
			float mean_u <- individual mean_of (each.global_op_u);
			float evol_u <- mean_u - mean_u_init;
			
			save  [
				scenario, mean_op, evol, mean_u, evol_u, individual count (each.state in ["adoption", "satisfied", "unsatisfied"]), length(with_new_arg)
			] type:"csv" to: myself.data_file header: false rewrite: false;
			
			save [
				""+scenario+","+myself.list_to_csv(mean_op_times.values)
			] type:"text" to:myself.time_file header: false rewrite: false;
			
		}
		
		cpt <- cpt + 1;
	}
}

experiment proportion  type:batch repeat: 10 until: (cycle > 10000) keep_seed: false {
	parameter "Scenario" var: prop_pop <- 0.1 among:[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0];
	
	int cpt <- 0;
	string directory <- "../results/scenario2/data/";
	string name <- "sans_arg_sans_soc";
	string file_name <- directory+name+".opinion.csv";
	string init_file_name <- directory+name+'.init.csv';
	string  data_file <- directory+name+'.table.csv';
	string  time_file <- directory+name+'.time.csv';
	
	string time_header{
		string res <- "";
		loop c from: 0 to: 10000 step: 500{
			res <- string(c)+",";
		}
		return res;
	}
	
	string list_to_csv(list l){
		string res <- '';
		loop e over:l{
			res <- res + string(e)+",";
		}
		return res;
	}
	
	reflex end_of_runs {
		int nb_simulations <- length(list(simulations));
		
		if cpt = 0 { 
			save "prop,[-1;-0.75[,[-0.75;-0.5[,[-0.5;-0.25[,[-0.25;0.0[,[0.0;0.25[,[0.25;0.5[,[0.5;0.75[,[0.75;1.0]" type:"text" to: file_name header:false rewrite: true;
			save "prop,[-1;-0.75[,[-0.75;-0.5[,[-0.5;-0.25[,[-0.25;0.0[,[0.0;0.25[,[0.25;0.5[,[0.5;0.75[,[0.75;1.0]" type:"text" to: init_file_name header:false rewrite: true;
			save "prop,mean_opinion,evolution,mean_uncertainty, evolution_u,nb_adoptants,nb_agents_with_new_arg" type:"text" to: data_file header:false rewrite: true;
			save "scenario,"+time_header() type:"text" to: time_file header:false rewrite: true;
		}
		
		
		ask simulations {
			save [ 	prop_pop, 	
					individual count (each.global_op < -0.75),
					individual count ((each.global_op >= -0.75) and (each.global_op < -0.5)),
					individual count ((each.global_op >= -0.5) and (each.global_op < -0.25)),
					individual count ((each.global_op >= -0.25) and (each.global_op < 0.0)),
					individual count ((each.global_op >= 0.0) and (each.global_op < 0.25)),
					individual count ((each.global_op >= 0.25) and (each.global_op < 0.5)),
					individual count ((each.global_op >= 0.5) and (each.global_op < 0.75)),
					individual count ((each.global_op >= 0.75))] 
					type:"csv" to: myself.file_name header: false rewrite: false;
			
			
			save [ 	prop_pop, 	
					individual count (each.init_ben < -0.75),
					individual count ((each.init_ben >= -0.75) and (each.init_ben < -0.5)),
					individual count ((each.init_ben >= -0.5) and (each.init_ben < -0.25)),
					individual count ((each.init_ben >= -0.25) and (each.init_ben < 0.0)),
					individual count ((each.init_ben >= 0.0) and (each.init_ben < 0.25)),
					individual count ((each.init_ben >= 0.25) and (each.init_ben < 0.5)),
					individual count ( (each.init_ben >= 0.5) and (each.init_ben < 0.75)),
					individual count ((each.init_ben >= 0.75))] 
					type:"csv" to: myself.init_file_name header: false rewrite: false;
					
			float mean_op <- individual mean_of (each.global_op);
			float evol <- mean_op - mean_op_init;
			float mean_u <- individual mean_of (each.global_op_u);
			float evol_u <- mean_u - mean_u_init;
			
			save  [
				prop_pop, mean_op, evol, mean_u, evol_u, individual count (each.state in ["adoption", "satisfied", "unsatisfied"]), length(with_new_arg)
			] type:"csv" to: myself.data_file header: false rewrite: false;
			
			save [
				""+scenario+","+myself.list_to_csv(mean_op_times.values)
			] type:"text" to:myself.time_file header: false rewrite: false;
			
		}
		
		cpt <- cpt + 1;
	}
}