/***
* Name: individual
* Author: francoisledoyen
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model diffusion_innovation_argumentation

species individual skills: [argumenting] control: fsm {
	list<individual> neighbors;
	map<string, float> source_confidence;
	
	graph global_argumentation_graph;
	
	int nb_relevents_args min:0;
	list<argument> relevents_args;
	
	string interest <- "maybe" among: ["no", "maybe", "yes"]; 
	
	bool informed -> {length(relevents_args) = nb_relevents_args};
	
	float social_op min: -1.0 max: 1.0;
	float social_op_u min: 0.0 max: 2.0;
	
	float indiv_ben min: -1.0 max: 1.0;
	float indiv_ben_u min: 0.0 max: 2.0;
	
	float global_op;
	float global_op_u;
	
	int time_in_pre_adoption <- 0;
	int time_in_adoption <- 0;
	int q <- 15;
	int p <- 10;
	
	list<argument> decision_args;
	
	float init_ben;

	state "not concerned" initial: true{
		transition to:"information request" when:interest in ["maybe", "yes"];
	}
	
	state "information request" {
		transition to:"no adoption" when: informed and interest in ["no"]; 
		transition to:"pre-adoption" when: informed and interest in ["yes"];
		transition to:"not concerned" when: !informed and interest in ["no"];
	}
	
	state "no adoption" {
		transition to:"pre-adoption" when: interest = "yes";
	} 	
	
	state "pre-adoption" {
		time_in_pre_adoption <- time_in_pre_adoption + 1;
		transition to:"adoption" when: interest in ["yes"] and time_in_pre_adoption >= p;
		transition to:"no adoption" when: interest in ["no", "maybe"];
		exit {
			time_in_pre_adoption <- 0;
		}
	}
	
	state adoption{
		time_in_adoption <- time_in_adoption + 1;
		transition to:satisfied when: time_in_adoption > q and is_satisfied();
		transition to:unsatisfied when: time_in_adoption > q and !is_satisfied();
		exit {
			time_in_adoption <- 0;
		}
	}
	
	state satisfied{}
	
	state unsatisfied{}
	
	bool is_satisfied{
		return flip(0.5);
	}
	
	reflex compute_interest {
		if (global_op + global_op_u < 0){
			interest <- "no";
		} else if (global_op - global_op_u > 0){
			interest <- "yes";
		} else {
			interest <- "maybe";	
		}
	}
	
	action compute_indiv_ben{
		pair decision <- make_decision();
		self.indiv_ben <- float(decision.value);
		self.decision_args <- decision.key;
	}
	
	action compute_indiv_ben_u{
		self.indiv_ben_u  <- 1 - mean (decision_args accumulate (source_confidence at each.source_type));
	}
	
	action compute_global_op{
		self.global_op <- mean(indiv_ben, social_op);
	}
	
	action compute_global_op_u{
		self.global_op_u <- mean(indiv_ben_u, social_op_u);
	}
	
	action compute_social_op{
		
	}
	
	action compute_social_op_u {
		
	}
		
	action add_arg(argument new_arg){
		if new_arg in relevents_args{
			relevents_args >> new_arg;
		} else if (length(relevents_args) = nb_relevents_args){
			argument to_be_suppr <- first(relevents_args);
			remove index: 0 from: relevents_args;
			do remove_argument(to_be_suppr);
		}
		relevents_args << new_arg;
		
		if !(new_arg in argumentation_graph.vertices){
			do add_argument(new_arg, global_argumentation_graph);
		} 
	}
	
	action search_information(graph<argument, unknown> arg_graph){
		loop i over: source_confidence sort_by each{
			unknown source <- source_confidence index_of i;
			list args <- collect(arg_graph.vertices, each.source_type = i) - self.argumentation_graph.vertices;
			if !empty(args){
				do add_arg(argument(one_of (args)));
				break;
			}
		}
	}
	
	action give_arguments(individual partner, list<argument> args) {
		ask partner{
			if (!empty(args)){
				loop a over: args {
					do add_arg(a);	
				}			
			}
		}
	}

	action discuss{
		list<argument> args <- select_arguments();
		loop neighbor over: rnd(0, length(neighbors)) among neighbors{
			if (similarity(neighbor) >= neighbor.global_op_u){
				
			}
			do give_arguments(neighbor, args);
			
		}
	}
	
	list<argument> select_arguments{
		list<argument> res;
		if self.state = 'not concerned'{
			res <- [];
		} else {
			res <- 1 among decision_args;
		}
		
		return res;
	}
 	
	float similarity(individual other){
		return min(self.social_op + self.social_op_u, other.social_op + other.social_op_u) - max(self.social_op - self.social_op_u, other.social_op - other.social_op_u);
	}
}