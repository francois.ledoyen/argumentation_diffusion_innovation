/**
* Name: test
* Based on the internal empty template. 
* Author: francoisledoyen
* Tags: 
*/


model test

global {
	init {
		map m <- map([2::"b", 1::"c", 3::"d"]);
		list l <- [1, 2, 3];
		save l to:"../results/test.txt" type:csv;
	}
}

/* Insert your model definition here */

experiment main type: gui;