/***
* Name: arggraphviewer
* Author: francoisledoyen
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model arggraphviewer

species graph_viewer{
	graph<argument, unknown> arg_graph;
	
	float node_size <- 1.0;
	float vertical_space <- 2.0;
	float horizontal_space <- 25.0;
	map<argument, node_arg> refs;
	init {
		write (length(arg_graph.edges));
		int n_p <- 10;
		int n_c <- 10;
		loop i from:1 to: length(arg_graph.vertices) {
			argument a <- arg_graph.vertices[i-1];
			create node_arg with:(
				arg:a,
				type:a.conclusion,
				location:{(a.conclusion = "+" ? 10 : 2*horizontal_space),(a.conclusion = "+" ? n_p : n_c)+vertical_space*node_size},
				neighbors:arg_graph neighbors_of a
			);
			if a.conclusion = "+"{
				n_p <- n_p+3;
			} else {
				n_c <- n_c+3;
			}
			add a::list(node_arg)[i-1] to: refs;
		}
	}
	
	aspect base{
		loop node over:node_arg{
			draw node.view color:node.color border: #black;
			draw string(node.arg) color: #black size: 2.0 at: {node.location.x, node.location.y};
			loop n over: node.neighbors{
				draw line(closest_points_with(node.view, refs[n].view)) color:#black end_arrow: 0.3;  
			} 
		}
	}

	
	species node_arg{
		argument arg;
		string type <- "+" among: ["+", "-"];
		rgb color <- type = "+" ? #green : #red;
		list<argument> neighbors <- [];
		geometry view;
		
		init {
			view <- circle(node_size);
		}
	}
}

experiment "test"{
	
}
/* Insert your model definition here */

