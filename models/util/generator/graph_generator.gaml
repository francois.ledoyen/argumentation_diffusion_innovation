/***
* Name: graphgenerator
* Author: francoisledoyen
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model graphgenerator

species argumentation_graph_generator {
	//// to be initialized
	int nb_args;
	float prop_P_args;
	bool proportion <- false;
	float prop_att_P_C;
	float prop_att_C_P;
	int nb_attacks;
	///////////////////
	list source_types <- ["Institution","Newspaper","Scientific report", "Social media"];
	list criteria <- ["Ecological", "Financial", "Productivity", "Social"];
	int card_P;
	int card_C;
	
	list<argument> P;
	list<argument> C;
	
	int nb_pro_att;
	int nb_con_att;
	
	
	
	graph<argument, unknown> argumentation_graph <- nil;
	
	init build_graph {
		do build_args;
		argumentation_graph <- directed(graph(P + C));
		if false {
			nb_pro_att <- int(prop_att_P_C*card_P*card_C);
			nb_con_att <- int(prop_att_C_P*card_P*card_C);
			loop e over: build_edges(nb_con_att, C, P)+build_edges(nb_pro_att, P, C){
				argumentation_graph <- argumentation_graph add_edge(pair(e));
			}
		} else {
			loop e over: nb_attacks among shuffle(build_all_edges()){
				argumentation_graph <- argumentation_graph add_edge(pair(e));
			}
		}
		
	}
	
	action build_args{
		card_P <- int(nb_args*prop_P_args);
		card_C <- nb_args - card_P;
		
		loop id from: 0 to: nb_args-1{
			argument a <- argument(["id"::string(id), 
									"conclusion"::(id<card_P ? "+" : "-"), 
									"source_type"::one_of(source_types), 
									"criteria"::[one_of(criteria)::1.0]]);
			
			(id<card_P ? P : C) << a; 
		}
	}
	
	list build_edges(int n, list<argument> start, list<argument> target){
		list edges <- [];
		loop s over: start {
			edges <- edges + accumulate(target, s::each);
		}
		return copy_between(shuffle(edges), 0, n);
	}
	
	list build_all_edges{
		list edges <- [];
		loop s over: P {
			edges <- edges + accumulate(C, s::each);
		}
		
		loop s over: C {
			
				edges <- edges + accumulate(P, s::each);
			
		}
		return edges; 
	}
}
