/***
* Name: individualgenerator
* Author: francoisledoyen
* Description: 
* Tags: Tag1, Tag2, TagN
***/

model individualgenerator

import "../../species/individual.gaml"

species individual_generator {
	graph<argument, unknown> global_args_graph;
	list source_types;
	list criteria;
	int nb_relevents_args <- 10;
	float min_criteria <- 0.0;
	float max_criteria <- 1.0;
	float max_conf_st <- 1.0;
	float min_conf_st <- 0.0;
	string network_density <- "high";
	
	
	action create_individuals(int n, int nb_args_p, int nb_args_c){
		
		create individual number: n {
			map sc <- myself.source_types as_map (each::rnd(myself.min_conf_st, myself.max_conf_st));
			map crit <- myself.criteria as_map (each::float(rnd(myself.min_criteria,myself.max_criteria)));
			self.source_confidence <- sc;
			self.crit_importance <- crit; 
			self.global_argumentation_graph <- myself.global_args_graph;
			self.argumentation_graph <- directed(graph([]));
			self.nb_relevents_args <- myself.nb_relevents_args;
			
			loop tuple over:[[nb_args_p,"+"], [nb_args_c,"-"]]{
				loop times: int(tuple at 0) {
 					do add_arg(one_of((myself.global_args_graph.vertices - argumentation_graph.vertices) where (each.conclusion = (tuple at 1))));
	 			}
	 		}	
		}
	}
	
//	action create_individuals(int n){
//		
//		create individual number: n{
//			self.nb_relevents_args <- myself.nb_relevents_args;
//			self.source_confidence <- myself.source_types as_map (each::rnd(myself.min_conf_st,myself.max_conf_st));
//			self.crit_importance <- myself.criteria as_map (each::float(rnd(myself.min_criteria,myself.max_criteria))); 
//			self.argumentation_graph <- directed(graph([]));
//			self.global_argumentation_graph <- myself.global_args_graph;
//			loop times: myself.nb_relevents_args {
//				argument arg <- one_of(myself.global_args_graph.vertices - self.argumentation_graph.vertices);
//				do add_arg(arg);
// 			}
//		}
//		
//	}
	
	action link_individuals{
		ask individual{
			neighbors <- rnd(1,(myself.network_density = "high") ? 10 : 4) among (individual - self);
		}
	}
}

