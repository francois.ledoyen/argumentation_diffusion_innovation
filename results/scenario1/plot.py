import matplotlib.pyplot as plt
import numpy as np
import pandas as pd


def plot_bar_chart_opinion(file, output, name_col):
    data = pd.read_csv(file, sep=",")

    scenario = list(np.unique(data[name_col].to_numpy()))
    legend = data.columns[1:]

    nchart = len(scenario)
    fig, ax = plt.subplots(3, 1, sharey='row', figsize=(15,10))
    plt.subplots_adjust(hspace=.5)
    
    for i in range(nchart):
        n = scenario[i]
        d = data.loc[data[name_col]==n].drop(axis=1, labels=name_col)

        mean = d.mean(axis=0).to_numpy()
        std_dev = d.std(axis=0).to_numpy()
        y = 0
        x = i
        
        x_pos = np.arange(len(legend))
        ax[x].bar(x_pos, mean, yerr=std_dev, align='center', alpha=0.5, ecolor='black', capsize=2)
        # ax.set_xlabel('Individual benefit of agents')

        # ax.set_ylabel('Proportion of population (%)')
        ax[x].set_title('{} agents'.format(n))
        ax[x].set_xticks(x_pos)
        ax[x].set_xticklabels(legend, fontsize=10, rotation=90)

        ax[x].yaxis.grid(True)

    # plt.show()
    plt.tight_layout()
    plt.savefig(output)


def plot_time(file, output, name_col):
    data = pd.read_csv(file, sep=",")
    legend = data.columns[1:]

    params = list(np.unique(data[name_col].to_numpy()))
    fig, ax = plt.subplots(figsize=(15,10))
    
    for p in params :
        d = data.loc[data[name_col]==n].drop(axis=1, labels=name_col)

        mean = d.mean(axis=0).to_numpy()
        x_pos = np.arange(len(legend))
        plt.plot(x_pos, mean, label=p)
        
        
        

    # plt.show()
    plt.tight_layout()
    plt.savefig(output)



if __name__ == '__main__':
    
    files = ["data/arg_pos.opinion.csv"]
    outputs = ["output/arg_pos.opinion.png"]
    for i in range(len(files)):
        plot_bar_chart_opinion(files[i], outputs[i], "scenario")

    




